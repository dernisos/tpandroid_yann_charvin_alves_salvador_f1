package com.example.yacharvin.tpandroid.Others;

import android.content.Context;
import android.content.Intent;

import com.example.yacharvin.tpandroid.Activity.DetailActivity;
import com.example.yacharvin.tpandroid.Java_Class.Movie;

public class MovieListener implements MyAdapter.OnMovieClickedListener {
    private Context context;

    public MovieListener(Context context){
        this.context = context;
    }

    @Override
    public void onMovieClicked(Movie movie){
        Intent intent = new Intent(context, DetailActivity.class);
        intent.putExtra("movie",movie);
        context.startActivity(intent);
    }
}
