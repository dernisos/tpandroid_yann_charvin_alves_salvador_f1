package com.example.yacharvin.tpandroid.Java_Class;

public class Geoloc {
    private float lat;
    private float longu;


    // Getter Methods

    public float getLat() {
        return lat;
    }

    public float getLongu() {
        return longu;
    }

    // Setter Methods

    public void setLat(float lat) {
        this.lat = lat;
    }

    public void setLong(float longu) {
        this.longu = longu;
    }
}