package com.example.yacharvin.tpandroid.Others;

import com.example.yacharvin.tpandroid.Java_Class.JSON_Class;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface CinemaApi {
    @GET("https://etudiants.openium.fr/pam/cine.json")
    Call<JSON_Class> getFile();
}
