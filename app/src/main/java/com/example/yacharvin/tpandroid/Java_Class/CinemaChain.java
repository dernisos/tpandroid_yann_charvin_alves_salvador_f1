package com.example.yacharvin.tpandroid.Java_Class;

public class CinemaChain {
    private float code;
    private String name;


    // Getter Methods

    public float getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    // Setter Methods

    public void setCode(float code) {
        this.code = code;
    }

    public void setName(String name) {
        this.name = name;
    }
}