package com.example.yacharvin.tpandroid.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.yacharvin.tpandroid.Java_Class.Movie;
import com.example.yacharvin.tpandroid.R;

import java.util.Objects;

public class DetailActivity extends AppCompatActivity {

    private Movie movie;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        Intent i =getIntent();
        movie = (Movie)i.getSerializableExtra("movie");
        TextView timeTextView = findViewById(R.id.Time);
        TextView realTextView = findViewById(R.id.Real);
        TextView actTextView = findViewById(R.id.Act);
        WebView videoWeb = findViewById(R.id.Pict);

        int hour = movie.getRuntime()/3600;
        int min = movie.getRuntime()/60 - hour*60;
        String time = Integer.toString(hour) + "h" + Integer.toString(min)+ " | ";
        for(int j = 0; j<movie.getGenre().length; j++){
            time = time+movie.getGenre()[j].getName();
            if(j<movie.getGenre().length-1){
                time += ", ";
            }
        }
        Objects.requireNonNull(getSupportActionBar()).setTitle(movie.getTitle());

        timeTextView.setText(time);
        realTextView.setText("Realisateur : "+movie.getCastingShort().getDirectors());
        actTextView.setText("Acteurs : "+movie.getCastingShort().getActors());
        if(movie.getTrailer() != null && movie.getTrailer().getHref()!=null) {
            String urlString[] = movie.getTrailer().getHref().split("v=");
            videoWeb.getSettings().setJavaScriptEnabled(true);
            videoWeb.setWebChromeClient(new WebChromeClient());
            videoWeb.loadData("<iframe width=\"100%\" height=\"100%\" src=\"https://www.youtube.com/embed/" + urlString[1] + "\" frameborder=\"0\" allowfullscreen=true></iframe>", "text/html", "utf-8");
        }
    }
}
