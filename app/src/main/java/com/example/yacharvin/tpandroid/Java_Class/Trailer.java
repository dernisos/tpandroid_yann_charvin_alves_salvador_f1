package com.example.yacharvin.tpandroid.Java_Class;

import java.io.Serializable;

public class Trailer implements Serializable {
    private String href;

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }
}
