package com.example.yacharvin.tpandroid.Java_Class;


import java.util.List;

public class JSON_Class {
    Place place;
    List< movieShowtimes > movieShowtimes;


    // Getter Methods

    public Place getPlace() {
        return place;
    }

    // Setter Methods

    public void setPlace(Place placeObject) {
        this.place = placeObject;
    }

    public List<movieShowtimes> getmovieShowtimes() {
        return movieShowtimes;
    }

    public void setMovieShowtimes(List<com.example.yacharvin.tpandroid.Java_Class.movieShowtimes> movieShowtimes) {
        this.movieShowtimes = movieShowtimes;
    }
}