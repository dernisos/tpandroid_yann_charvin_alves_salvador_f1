package com.example.yacharvin.tpandroid.Java_Class;

public class Picture {
    private String path;
    private String href;


    // Getter Methods

    public String getPath() {
        return path;
    }

    public String getHref() {
        return href;
    }

    // Setter Methods

    public void setPath(String path) {
        this.path = path;
    }

    public void setHref(String href) {
        this.href = href;
    }
}