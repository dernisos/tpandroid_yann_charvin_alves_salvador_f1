package com.example.yacharvin.tpandroid.Java_Class;


import java.io.Serializable;

public class Movie implements Serializable
{
    private CastingShort castingShort;
    private Release release;
    private Genre[] genre;
    private String title;
    private Poster poster;
    private int runtime;
    private Trailer trailer;

    public Trailer getTrailer() {
        return trailer;
    }

    public void setTrailer(Trailer trailer) {
        this.trailer = trailer;
    }

    public int getRuntime() {
        return runtime;
    }

    public void setRuntime(int runtime) {
        this.runtime = runtime;
    }

    public CastingShort getCastingShort ()
    {
        return castingShort;
    }

    public void setCastingShort (CastingShort castingShort)
    {
        this.castingShort = castingShort;
    }

    public Release getRelease ()
    {
        return release;
    }

    public void setRelease (Release release)
    {
        this.release = release;
    }

    public Genre[] getGenre ()
    {
        return genre;
    }

    public void setGenre (Genre[] genre)
    {
        this.genre = genre;
    }

    public String getTitle ()
    {
        return title;
    }

    public void setTitle (String title)
    {
        this.title = title;
    }

    public Poster getPoster ()
    {
        return poster;
    }

    public void setPoster (Poster poster)
    {
        this.poster = poster;
    }

}



