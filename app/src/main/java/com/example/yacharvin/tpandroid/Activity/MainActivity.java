package com.example.yacharvin.tpandroid.Activity;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.widget.TextView;

import com.example.yacharvin.tpandroid.Java_Class.JSON_Class;
import com.example.yacharvin.tpandroid.Java_Class.Movie;
import com.example.yacharvin.tpandroid.Others.ApiHelper;
import com.example.yacharvin.tpandroid.Others.MovieListener;
import com.example.yacharvin.tpandroid.Others.MyAdapter;
import com.example.yacharvin.tpandroid.R;

import jp.wasabeef.recyclerview.animators.SlideInUpAnimator;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity{

    RecyclerView mRecyclerView;
    RecyclerView.LayoutManager mLayoutManager;
    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mRecyclerView = (RecyclerView)findViewById(R.id.RecyclV);
        mRecyclerView.setItemAnimator(new SlideInUpAnimator());
        mLayoutManager = new LinearLayoutManager(this);
        context = this;
        ApiHelper.getInstance().getCinemaApi().getFile().enqueue(new Callback<JSON_Class>() {
            @Override
            public void onResponse(Call<JSON_Class> call, Response<JSON_Class> response) {
                if(response.isSuccessful()){
                    JSON_Class jsCine = new JSON_Class();
                    jsCine.setPlace(response.body().getPlace());
                    jsCine.setMovieShowtimes(response.body().getmovieShowtimes());
                    MyAdapter adapter = new MyAdapter(jsCine.getmovieShowtimes(), new MovieListener(context));
                    mRecyclerView.setAdapter(adapter);
                    mRecyclerView.setLayoutManager(mLayoutManager);
                }
                else{
                    //Ajouter exception
                }
            }

            @Override
            public void onFailure(Call<JSON_Class> call, Throwable t) {

            }
        });
    }

}
