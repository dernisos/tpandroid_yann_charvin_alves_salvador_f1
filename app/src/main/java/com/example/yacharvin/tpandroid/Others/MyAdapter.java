package com.example.yacharvin.tpandroid.Others;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.yacharvin.tpandroid.Java_Class.Movie;
import com.example.yacharvin.tpandroid.Java_Class.movieShowtimes;
import com.example.yacharvin.tpandroid.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder> {

    private OnMovieClickedListener listener;

    public class ViewHolder extends RecyclerView.ViewHolder{
        public TextView nameFilm;
        public TextView genreFilm;
        public ImageView imageFilm;

        public ViewHolder(View itemView){
            super(itemView);

            nameFilm = (TextView)itemView.findViewById(R.id.nom_film);
            genreFilm = (TextView)itemView.findViewById(R.id.genre_film);
            imageFilm = (ImageView)itemView.findViewById(R.id.image_film);
        }
    }
    private List<movieShowtimes > mymovieShowtimes;

    public MyAdapter(List<movieShowtimes> mvSt, OnMovieClickedListener clickedListener){
        mymovieShowtimes = mvSt;
        listener = clickedListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        View movieView = inflater.inflate(R.layout.item_list, parent, false);

        ViewHolder viewHolder = new ViewHolder(movieView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(MyAdapter.ViewHolder viewHolder, int position) {
        final movieShowtimes mvst = mymovieShowtimes.get(position);

        TextView nametxt = viewHolder.nameFilm;
        TextView genreTxt = viewHolder.genreFilm;
        nametxt.setText(mvst.getOnShow().getMovie().getTitle());
        genreTxt.setText(mvst.getOnShow().getMovie().getGenre()[0].getName());
        ImageView filmimg = viewHolder.imageFilm;
        Picasso.get().load(mvst.getOnShow().getMovie().getPoster().getHref()).into(filmimg);
        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               listener.onMovieClicked(mvst.getOnShow().getMovie());
            }
        });
    }

    @Override
    public int getItemCount() {
        return mymovieShowtimes.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        TextView titre;
        TextView genre;
        ImageView img;

        MyViewHolder(View v){
            super(v);
            titre = v.findViewById(R.id.nom_film);
            genre = v.findViewById(R.id.genre_film);
            img = v.findViewById(R.id.image_film);
        }

        @Override
        public void onClick(View v){

        }
    }

    public interface OnMovieClickedListener{
        void onMovieClicked(Movie movie);
    }
}
