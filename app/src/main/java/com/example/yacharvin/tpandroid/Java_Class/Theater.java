package com.example.yacharvin.tpandroid.Java_Class;

import java.util.ArrayList;

public class Theater {
    private String code;
    private String name;
    private String address;
    private String postalCode;
    private String city;
    private String area;
    Picture picture;
    CinemaChain cinemaChain;
    private float hasPRMAccess;
    private float openToSales;
    Geoloc geoloc;
    ArrayList< Object > link = new ArrayList < Object > ();


    // Getter Methods

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public String getCity() {
        return city;
    }

    public String getArea() {
        return area;
    }

    public Picture getPicture() {
        return picture;
    }

    public CinemaChain getCinemaChain() {
        return cinemaChain;
    }

    public float getHasPRMAccess() {
        return hasPRMAccess;
    }

    public float getOpenToSales() {
        return openToSales;
    }

    public Geoloc getGeoloc() {
        return geoloc;
    }

    // Setter Methods

    public void setCode(String code) {
        this.code = code;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public void setPicture(Picture pictureObject) {
        this.picture = pictureObject;
    }

    public void setCinemaChain(CinemaChain cinemaChainObject) {
        this.cinemaChain = cinemaChainObject;
    }

    public void setHasPRMAccess(float hasPRMAccess) {
        this.hasPRMAccess = hasPRMAccess;
    }

    public void setOpenToSales(float openToSales) {
        this.openToSales = openToSales;
    }

    public void setGeoloc(Geoloc geolocObject) {
        this.geoloc = geolocObject;
    }
}
